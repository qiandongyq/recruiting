# Special Notes

## In case you want to use docker to run mongoDB, you can follow below steps.

- `yarn db:start` or you can run `yarn dev` to start both database and application
- find out container id by `docker container ls`, copy the container ID
- Copy fixture file into container `docker cp fixtures/stats.collection.js {docker container id here}:/`
- run `docker exec -it {docker container id here} /bin/bash`
- run `mongoimport --db mongo --collection stats --drop --file stats.collection.json`

# 2019 Q1 Backend Entry Submission

## Summary

This is not an exhaustive examination of your skillset, but rather we are trying to ensure a basic understanding of the subject matter. You are tasked with creating a simple RESTful CRUD API to a mongo collection, as well as create a simple aggregate query in mongo.
There are no tricks here, nor are we looking to see if you'll cover every minute edge case. You may use any library/framework you like. We expect this test to take about an hour of your time (assuming you already have mongo, and node installed on your machine etc...)

In this repo you will find a json dump of the `stats` collection. You can find it in `./fixtures/stats.collection.json`. Ensure that you have a running Mongo db, and import this collection in it before you start the test.

The `stats` collection follows this schema:

```javascript
{
  _id: ObjectId,
  latest: Boolean,
  schema_version: Number,
  asset_type: String,
  asset_id: String,
  stat: String,
  value: Any,
  created_at: Date,
  updated_at: Date,
}
```

### Final notes

We should be able to run your submission by executing the command `yarn start` or `npm run start`. We should also be able to have all the modules we need by executing the command `yarn` or `npm install`

Make sure you take a look at the submission instructions at the bottom after you've completed the test.

## 1. Node RESTful CRUD server

Create a Node server, that provides RESTful CRUD access to the `stats` collection through the route `collection/stats/`.

To be more specific:

- **POST** `collection/stats/`
- **GET** `collection/stats/`
- **GET** `collection/stats/{id}`
- **PUT** `collection/stats/{id}`
- **PATCH** `collection/stats/{id}`
- **DELETE** `collection/stats/{id}`

All routes should respond with either:

- the newly created record
- an updated/patched record
- a found record/ array of records
  - 404 in the case it doesn't exist
- the newly deleted record

You need not implement multi-item transactions/actions.

### Bonus

implement a `soft delete` feature, where instead of removing a document, you just mark it as `deleted` in the document. If a document is marked deleted it should return 404 when queried.

## 2. Aggregate query

Create a route `report/recent_stats_by_channel/` on the server that returns the result of an aggregate query that groups the stats by channel, and shows the 10 most recent records for the stats: `youtube_subscribers`, and `youtube_posts_30d`.

The expected end result should look something like this:

```javascript
[
  {
    _id: '<channel_id>',
    subscribers: [
      { value: Number, date: Date }
      // most recent 10 records
    ],
    posts_30d: [
      { value: Number, date: Date }
      // most recent 10 records
    ]
  }
];
```

## 3. Contact route

Create a route `utils/contact_me` on the server that responds with your personal contact information following this format:

```javascript
{
  name: String,
  email: String,
  phone: String,

  // you can leave any note you like here
  // like prefered hours to call/email etc...
  note: String,

  // you can leave a URL, otherwise we'll look in `./fixtures/`
  resume: String,
}
```

---

## Submission instructions

We recommend you fork this repo `git@gitlab.com:alfan/recruiting.git`, and either ensure it's publically accessible, or extend access to our CTO `"Amin El-Naggar <amin@alfangroup.com>"` as a member in your forked repo.
