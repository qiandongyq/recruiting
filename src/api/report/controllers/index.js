import * as services from '../services';

export const getRecentStatsByChannel = (req, res) =>
  services.getRecentStatsByChannel(req, res);
