import { Router } from 'express';

const router = Router();
const controllers = require('../controllers');

router.get('/recent_stats_by_channel', controllers.getRecentStatsByChannel);

export default router;
