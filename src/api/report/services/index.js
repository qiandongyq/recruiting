import Boom from 'boom';
import _ from 'lodash';

export const getRecentStatsByChannel = async (req, res) => {
  try {
    const result = [];
    const stats = await req.context.models.Stats.find();
    const groupByChannel = _.groupBy(stats, 'asset_id');

    _.forEach(groupByChannel, g => {
      const group = {};
      group._id = g[0].asset_id;
      const groupStats = _.groupBy(g, 'stat');
      _.forEach(groupStats, stats => {
        group[stats[0].stat] = _.sortBy(stats, stats.createdAt)
          .reverse()
          .slice(0, 10);
      });
      result.push(group);
    });

    return res.send(result);
  } catch (err) {
    return res.send(Boom.badImplementation(err));
  }
};
