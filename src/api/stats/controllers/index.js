import * as services from '../services';

export const getStats = (req, res) => services.getStats(req, res);

export const createStats = (req, res) => services.createStats(req, res);

export const getStatsById = (req, res) => services.getStatsById(req, res);

export const updateAndPatchStatsById = (req, res) =>
  services.updateAndPatchStatsById(req, res);

export const deleteStatsById = (req, res) => services.deleteStatsById(req, res);
