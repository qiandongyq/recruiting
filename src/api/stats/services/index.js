import mongoose from 'mongoose';
import Boom from 'boom';
import _ from 'lodash';

export const getStats = async (req, res) => {
  try {
    const stats = await req.context.models.Stats.find();
    return res.send(stats);
  } catch (err) {
    return res.send(Boom.badImplementation(err));
  }
};

export const createStats = async (req, res) => {
  const values = req.body;
  if (_.isEmpty(values)) return res.send(Boom.badRequest('Missing paramters'));
  try {
    const stats = await req.context.models.Stats.create(values);
    if (!stats) return res.send(Boom.notFound('Create Stats is failed'));
    return res.send(stats);
  } catch (err) {
    return res.send(Boom.badImplementation(err));
  }
};

export const getStatsById = async (req, res) => {
  const { id } = req.params;
  if (!id) return res.send(Boom.badRequest('Missing status id'));
  if (!mongoose.Types.ObjectId.isValid(id))
    return res.send(Boom.badData('Stats ID: ${id} is not valid ID'));
  try {
    const stats = await req.context.models.Stats.findById({
      _id: id
    });
    if (!stats)
      return res.send(Boom.notFound(`Stats with ID: ${id} is not found`));
    return res.send(stats);
  } catch (err) {
    return res.send(Boom.badImplementation(err));
  }
};

export const updateAndPatchStatsById = async (req, res) => {
  const { id } = req.params;
  if (!id) return res.send(Boom.badRequest('Missing status id'));
  if (!mongoose.Types.ObjectId.isValid(id))
    return res.send(Boom.badData('Stats ID: ${id} is not valid ID'));
  const values = req.body;
  try {
    const stats = await req.context.models.Stats.findOneAndUpdate(
      { _id: id },
      values,
      { new: true }
    );
    if (!stats)
      return res.send(Boom.notFound(`Stats with ID: ${id} is not found`));
    return res.send(stats);
  } catch (err) {
    return res.send(Boom.badImplementation(err));
  }
};

export const deleteStatsById = async (req, res) => {
  const { id } = req.params;
  if (!id) return res.send(Boom.badRequest('Missing status id'));
  if (!mongoose.Types.ObjectId.isValid(id))
    return res.send(Boom.badData('Stats ID: ${id} is not valid ID'));

  try {
    const stats = await req.context.models.Stats.findById({ _id: id });
    if (!stats) {
      return res.send(Boom.notFound(`Stats with ID: ${id} is not found`));
    }
    await req.context.models.Stats.delete({ _id: id });
    return res.send({
      ...stats._doc,
      deleted: true,
      deletedAt: new Date()
    });
  } catch (err) {
    return res.send(Boom.badImplementation(err));
  }
};
