import mongoose from 'mongoose';
import mongoose_delete from 'mongoose-delete';

const statsSchema = new mongoose.Schema({
  latest: Boolean,
  schema_version: Number,
  asset_type: String,
  asset_id: String,
  stat: String,
  value: mongoose.Mixed
});

statsSchema.set('timestamps', true);

// soft delete with override all method to not include deleted document
statsSchema.plugin(mongoose_delete, {
  deletedAt: true,
  overrideMethods: 'all'
});

const Stats = mongoose.model('Stats', statsSchema);

export default Stats;
