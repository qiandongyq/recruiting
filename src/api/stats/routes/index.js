import { Router } from 'express';

const router = Router();
const controllers = require('../controllers');

router.get('/', controllers.getStats);

router.post('/', controllers.createStats);

router.get('/:id', controllers.getStatsById);

router.put('/:id', controllers.updateAndPatchStatsById);

router.patch('/:id', controllers.updateAndPatchStatsById);

router.delete('/:id', controllers.deleteStatsById);

export default router;
