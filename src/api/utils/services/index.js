import Boom from 'boom';
import _ from 'lodash';

export const getContactInfo = async (req, res) => {
  try {
    return res.send({
      name: 'Dong Qian',
      email: 'qiandongyq@gmail.com',
      phone: '226-972-6088',
      note:
        "There is one special note in README for mongodb setup and import fixtrue(I used docker). There is no validation on schema and api paramters.  I didn't find any requirement about validation, but I do have knowledge how to handle the validation on schema and endpoint. Look forward to hear from you, thanks",
      resume: 'In /fixtures'
    });
  } catch (err) {
    return res.send(Boom.badImplementation(err));
  }
};
