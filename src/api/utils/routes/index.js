import { Router } from 'express';

const router = Router();
const controllers = require('../controllers');

router.get('/contact_me', controllers.getContactInfo);

export default router;
