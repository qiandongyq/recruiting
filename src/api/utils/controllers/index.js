import * as services from '../services';

export const getContactInfo = (req, res) => services.getContactInfo(req, res);
