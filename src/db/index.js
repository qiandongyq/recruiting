import mongoose from 'mongoose';
import Stats from '../api/stats/models';

const connectToDatabase = () => {
  return mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true });
};

const models = { Stats };

export { connectToDatabase };

export default models;
