import express from 'express';
import cors from 'cors';

import 'dotenv/config';
import statsRouter from './api/stats/routes';
import reportRouter from './api/report/routes';
import utilsRouter from './api/utils/routes';

import models, { connectToDatabase } from './db';

const app = express();

/* Appliy Middleware */
app.use(express.urlencoded({ extended: true }));
// Attch models context
app.use(async (req, res, next) => {
  req.context = {
    models
  };
  next();
});

// Enable cors
app.use(cors());

/* Routes */
app.use('/collection/stats', statsRouter);
app.use('/report', reportRouter);
app.use('/utils', utilsRouter);

// Connect to database and start server
connectToDatabase()
  .then(async () => {
    if (process.env.EARSEDATABASEONSYNC) {
      await Promise.all([models.Stats.deleteMany({})]);
    }

    app.listen(process.env.PORT, () =>
      console.log(`JOb test application running on port ${process.env.PORT}!`)
    );
  })
  .catch(err => console.log(err));
